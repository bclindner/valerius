module gitlab.com/bclindner/valerius/v0.3

require (
	github.com/bclindner/iasipgenerator v0.0.0-20181218024440-9e995f4ca2d0
	github.com/bwmarrin/discordgo v0.19.0
	github.com/gregjones/httpcache v0.0.0-20181110185634-c63ab54fda8f
	github.com/sirupsen/logrus v1.2.0
	golang.org/x/crypto v0.0.0-20181203042331-505ab145d0a9 // indirect
	golang.org/x/sys v0.0.0-20181217223516-dcdaa6325bcb // indirect
)
